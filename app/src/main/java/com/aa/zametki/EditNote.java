package com.aa.zametki;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import static com.aa.zametki.R.id.tvDate;

public class EditNote extends AppCompatActivity {

    int positionNote=11111111;
    EditText textee;
    Button btnSave;
    ImageButton btnDel;

    //date/time
    CheckBox checkDT;
    DatePicker tvDate;
    TimePicker tvTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        textee = (EditText) findViewById(R.id.editText22);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnDel = (ImageButton) findViewById(R.id.btnDel);


        Intent data = getIntent();
        positionNote = data.getIntExtra("pos", 0);

        textee.setText(NotesList.notesL.get(positionNote).nameNotes);

        //data/time
        checkDT = (CheckBox) findViewById(R.id.flagClick);
        tvDate = (DatePicker) findViewById(R.id.tvDate);
        tvTime =(TimePicker) findViewById(R.id.tvTime);
        tvTime.setIs24HourView(true);



        if(checkDT.isChecked() == false){
            tvDate.setVisibility(View.GONE);
            tvTime.setVisibility(View.GONE);
        }

        checkDT.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                tvDate.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);
                if(isChecked == false) {
                    tvDate.setVisibility(View.GONE);
                    tvTime.setVisibility(View.GONE);
                }
            }}
        );


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = null;
                if(checkDT.isChecked() == true){
                    Calendar cal  = Calendar.getInstance();
                    String dttStart = String.valueOf(tvDate.getDayOfMonth()+"."+(tvDate.getMonth()+1)+'.' + tvDate.getYear());
                    DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
                    try {
                        cal.setTime(df.parse(dttStart));
                        date = new Date(cal.getTimeInMillis()+tvTime.getCurrentHour()*3600000+ tvTime.getCurrentMinute()*60000);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    remir(textee.getText().toString(),date.toString()+" | "+String.valueOf(tvTime.getCurrentHour())+':'+ String.valueOf(tvTime.getCurrentMinute()));
                }
                else date = new Date(Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis());
                NotesList.notesL.get(positionNote).nameNotes = textee.getText().toString();
                NotesList.notesL.get(positionNote).dateNotes = date;
                NotesList.notesL.get(positionNote).minOfday = tvTime.getCurrentHour()*3600000+ tvTime.getCurrentMinute()*60000;
                Intent intent = new Intent(EditNote.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotesList.DelNote(positionNote);
                Intent intent = new Intent(EditNote.this, MainActivity.class);
                startActivity(intent);
            }
        });

        if(NotesList.notesL.get(positionNote).reminder == true)
            checkDT.setChecked(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_forward) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            if(checkDT.isChecked()==true)
                sendIntent.putExtra(Intent.EXTRA_TEXT, NotesList.notesL.get(positionNote).nameNotes + '\n' + "Date/time: " + NotesList.notesL.get(positionNote).dateNotes);
            else sendIntent.putExtra(Intent.EXTRA_TEXT, NotesList.notesL.get(positionNote).nameNotes);

            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, ""));



            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        positionNote = data.getIntExtra("pos", 0);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void remir(String title, String date) {
        Context context = getApplicationContext();

        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Resources res = context.getResources();
        Notification.Builder builder = new Notification.Builder(context);

        builder.setContentIntent(contentIntent)
                .setAutoCancel(false)
                .setSmallIcon(R.mipmap.ic_launcher)

                .setTicker("New note...")
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(date);
        Notification notification = builder.build();

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

}
