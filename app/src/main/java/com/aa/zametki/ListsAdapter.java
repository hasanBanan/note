package com.aa.zametki;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;



public class ListsAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Notes> objects;

    ListsAdapter(Context context, ArrayList<Notes> notes) {
        ctx = context;
        objects = notes;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return objects.size();
    }


    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);
        }

        Notes p = getNotes(position);

        ((TextView) view.findViewById(R.id.textView)).setText(p.nameNotes);
        int min = p.minOfday;
        ((TextView) view.findViewById(R.id.textView2)).setText(p.dateNotes + " / " + min/60 + ":"  + min%60);
        //if(p.listImages.isEmpty() == false)

        return view;
    }


    Notes getNotes(int position) {
        return ((Notes) getItem(position));
    }



}
