package com.aa.zametki;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Random;

import static com.aa.zametki.R.id.parent;

/**
 * Created by Arsen on 22.09.2017.
 */

public class Notes {
    ArrayList<Bitmap> listImages = new ArrayList<Bitmap>();
    ArrayList<String> imagePath = new ArrayList<String>();
    ArrayList<Uri> imageUri = new ArrayList<Uri>();
    public String nameNotes;
    public Date dateNotes;
    public int minOfday;
    public boolean reminder;
    public int notId = 0;

    Notes(String notes, Date date, boolean rem) {
        //listImages = uri;
        nameNotes = notes;
        dateNotes = date;
        reminder = rem;

        if(reminder == true){
            notId = (int)(Math.random()%1000);
        }
        minOfday = (int) ((date.getTime()/60000 + 180)%1440);
    }



}
