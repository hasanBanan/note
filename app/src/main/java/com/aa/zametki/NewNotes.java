package com.aa.zametki;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class NewNotes extends AppCompatActivity {

    private LinearLayoutManager horizontalLLM;
    RecyclerView recyclerView;
    private RecyclerAdapter adapter;

    ArrayList<Bitmap> imgs = new ArrayList<Bitmap>();
    ArrayList<String> imagePath = new ArrayList<String>();
    ArrayList<Uri> imageUri = new ArrayList<>();

    private final int CAMERA_RESULT = 0;
    private final int GALLERY_REQUEST = 1;

    ImageButton butPhoto;
    ImageButton butGal;
    Button butAdd;
    EditText textNote;

    //date/time
    CheckBox checkDT;
    DatePicker tvDate;
    TimePicker tvTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.listImage);

        butPhoto = (ImageButton) findViewById(R.id.button);
        butGal = (ImageButton) findViewById(R.id.button2);
        butAdd = (Button) findViewById(R.id.btnAdd);
        textNote = (EditText) findViewById(R.id.editText2);

        horizontalLLM = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLLM);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);

        //data/time
        checkDT = (CheckBox) findViewById(R.id.flagClick);
        tvDate = (DatePicker) findViewById(R.id.tvDate);
        tvTime =(TimePicker) findViewById(R.id.tvTime);
        tvTime.setIs24HourView(true);

        if(checkDT.isChecked() == false){
            tvDate.setVisibility(View.GONE);
            tvTime.setVisibility(View.GONE);

        }

        checkDT.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                tvDate.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);
                if(isChecked == false) {
                    tvDate.setVisibility(View.GONE);
                    tvTime.setVisibility(View.GONE);
                }
            }}
        );

        butPhoto.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view){

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_RESULT);

            }
        });

        butGal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });

        butAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Date date = null;
                Intent intent = new Intent();

                NotesList.setFlag();
                if(checkDT.isChecked() == true){//"2010-10-15T09:27:37Z"

                    Calendar cal  = Calendar.getInstance();
                    String dttStart = String.valueOf(tvDate.getDayOfMonth()+"."+(tvDate.getMonth()+1)+'.' + tvDate.getYear());
                    DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
                    try {
                        cal.setTime(df.parse(dttStart));
                        date = new Date(cal.getTimeInMillis()+tvTime.getCurrentHour()*3600000+ tvTime.getCurrentMinute()*60000);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    remir(textNote.getText().toString(),date.toString()+" | "+String.valueOf(tvTime.getCurrentHour())+':'+ String.valueOf(tvTime.getCurrentMinute()));
                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                }
                else date = new Date(Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis());

                NotesList.notesL.add(new Notes(textNote.getText().toString(), date, checkDT.isChecked()));
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }

    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
        private ArrayList<Bitmap> items = new ArrayList<>();
        //private List<CustomContact> listAddUsers;
        void addAll(ArrayList<Bitmap> images) {
            int pos = getItemCount();
            items = images;
            notifyItemRangeInserted(pos, this.items.size());
        }

        @Override
        public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
            return new RecyclerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolder holder, int position) {
            holder.bind(items.get(position));
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imageView);
        }

        void bind(Bitmap bit) {
            image.setImageBitmap(bit);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_forward) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            if(checkDT.isChecked() == true) {
                Date date = null;
                Intent intent = new Intent();
                Calendar cal  = Calendar.getInstance();
                String dttStart = String.valueOf(tvDate.getDayOfMonth()+"."+(tvDate.getMonth()+1)+'.' + tvDate.getYear());
                DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
                try {
                    cal.setTime(df.parse(dttStart));
                    date = new Date(cal.getTimeInMillis()+tvTime.getCurrentHour()*3600000+ tvTime.getCurrentMinute()*60000);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                sendIntent.putExtra(Intent.EXTRA_TEXT, textNote.getText().toString() + '\n' + "Date/time: " + date+ " / " +tvTime.getCurrentHour()+':'+tvTime.getCurrentMinute());
            }
            else sendIntent.putExtra(Intent.EXTRA_TEXT, textNote.getText().toString());
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, ""));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_RESULT && requestCode != -1 ) {
            Uri selectedImage = data.getData();
            imageUri.add(selectedImage);
            imagePath.add(UriToBit(selectedImage));
            try {
                Bitmap thumbnailBitmap = (Bitmap) data.getExtras().get("data");
                imgs.add(thumbnailBitmap);
                adapter.addAll(imgs);
            } catch (Throwable e) {
                e.printStackTrace();
            }


            //mImageView.setImageBitmap(thumbnailBitmap);
        }

        if(requestCode == GALLERY_REQUEST){
            super.onActivityResult(requestCode, resultCode, data);

            Bitmap bitmap = null;
            switch(requestCode) {
                case GALLERY_REQUEST:
                    if(resultCode == RESULT_OK){
                        Uri selectedImage = data.getData();
                        imageUri.add(selectedImage);
                        imagePath.add(UriToBit(selectedImage));

                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        imgs.add(bitmap);
                        adapter.addAll(imgs);
                    }
            }
        }
    }

    private String UriToBit(Uri uri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, proj, null, null, null);
        int index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(index);
    }

    public void remir(String title, String date) {
        Context context = getApplicationContext();

        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Resources res = context.getResources();
        Notification.Builder builder = new Notification.Builder(context);

        builder.setContentIntent(contentIntent)
                .setAutoCancel(false)
                .setSmallIcon(R.mipmap.ic_launcher)
                // большая картинка
                //.setTicker(res.getString(R.string.warning)) // текст в строке состояния
                .setTicker("New note...")
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                //.setContentTitle(res.getString(R.string.notifytitle)) // Заголовок уведомления
                .setContentTitle(title)
                //.setContentText(res.getString(R.string.notifytext))
                .setContentText(date); // Текст уведомления

        // Notification notification = builder.getNotification(); // до API 16
        Notification notification = builder.build();

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

}
