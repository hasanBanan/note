package com.aa.zametki;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity  {


    //ArrayList<Notes> notes = new ArrayList<Notes>();
    ArrayList<String> Path = new ArrayList<String>();
    ListsAdapter listAdapter;
    TextView text;
    String JStr;
    ListView lvMain;

    SharedPreferences sPref;

    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.info);

        sPref = getPreferences(MODE_PRIVATE);
        JStr = sPref.getString("note", "");

        if(JStr.length()>12 && NotesList.notesL.size()<1 && NotesList.flag == true) {
            NotesList.notesL = new Gson().fromJson(JStr, new TypeToken<ArrayList<Notes>>() {
            }.getType());
            NotesList.setFlag();

        }
        mImageView = (ImageView) findViewById(R.id.imageView);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final long x = System.currentTimeMillis();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(cameraIntent, CAMERA_RESULT);
                Intent intent = new Intent(MainActivity.this, NewNotes.class);
                startActivityForResult(intent, 1);
                //Snackbar.make(view, String.valueOf((Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis()/60000 + 180)%1440), Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }
});



        lvMain = (ListView) findViewById(R.id.lstV);

        SharedPreferences prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {
                Intent intent = new Intent(MainActivity.this, EditNote.class);
                intent.putExtra("pos", position);
                intent.putExtra("js", JStr);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data == null) {return;}
        NotesList.notesL = NotesList.notesL;
        listAdapter = new ListsAdapter(this, NotesList.notesL);
        ListView lvMain = (ListView) findViewById(R.id.lstV);
        lvMain.setAdapter(listAdapter);
        if(NotesList.notesL.isEmpty() != true)
            text.setVisibility(View.INVISIBLE);


    }



    @Override
    protected void onResume() {

        if(NotesList.notesL.isEmpty() != true)
            text.setVisibility(View.INVISIBLE);
        NotesList.notesL = NotesList.notesL;
        listAdapter = new ListsAdapter(this, NotesList.notesL);
        lvMain.setAdapter(listAdapter);
        super.onResume();
    }

    @Override
    protected void onStop() {
        String json = new Gson().toJson(NotesList.notesL);
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("note", json);
        ed.commit();
        super.onStop();
    }

}
